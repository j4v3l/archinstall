#!/bin/bash
clear
cat <<"EOF"
 ____  _     _        ____       _               
|  _ \(_)___| | __   / ___|  ___| |_ _   _ _ __  
| | | | / __| |/ /___\___ \ / _ \ __| | | | '_ \ 
| |_| | \__ \   <_____|__) |  __/ |_| |_| | |_) |
|____/|_|___/_|\_\   |____/ \___|\__|\__,_| .__/ 
                                          |_|    
EOF
echo "by J4V3L (2023)"
echo "-----------------------------------------------------"
echo "Setting up the base system..."

# Exit script on any error
set -e

# Install necessary tools if not present
install_if_not_present() {
  local package=$1
  if ! command -v "$package" &>/dev/null; then
    echo "Installing $package..."
    if ! pacman -Sy --noconfirm "$package"; then
      echo "Failed to install $package. Exiting."
      exit 1
    fi
  fi
}

# Install parted if not installed
install_if_not_present parted

# Load Keyboard Layout
if ! loadkeys us; then
  echo "Failed to load keyboard layout. Exiting."
  exit 1
fi

# Increase Font Size (optional)
if ! setfont ter-p20b; then
  echo "Failed to set font size. Continuing with default font."
fi

# Check Internet Connection
if ping -c 4 www.archlinux.org &>/dev/null; then
  echo "Internet Access is Available."
else
  echo "Internet connection not established. Please check your network settings."
  exit 1
fi

# List Partitions and prompt for the disk
echo "Listing available partitions..."
lsblk
echo "-----------------------------------------------------"
read -p "Enter the disk to partition (e.g., /dev/sda): " disk

# Validate disk input
if [[ ! -b "$disk" ]]; then
  echo "Invalid disk: $disk. Exiting."
  exit 1
fi

# Partitioning with parted
echo "Creating partitions on $disk..."
parted "$disk" --script mklabel gpt
parted "$disk" --script mkpart ESP fat32 1MiB 513MiB
parted "$disk" --script set 1 boot on
parted "$disk" --script mkpart primary ext4 513MiB 100%

# Run installation scripts from the current directory
echo "Starting installation script..."
if ! ./1-install.sh; then
  echo "Installation script failed. Exiting."
  exit 1
fi

clear
echo "Script executed successfully."
