#!/bin/bash
clear
cat <<"EOF"
  ____ _   _ ____   ___   ___ _____ 
 / ___| | | |  _ \ / _ \ / _ \_   _|
| |   | |_| | |_) | | | | | | || |  
| |___|  _  |  _ <| |_| | |_| || |  
 \____|_| |_|_| \_\\___/ \___/ |_|  
                                    
EOF
echo "by J4V3L (2023)"
echo "-----------------------------------------------------"

# Exit on any error
set -e

# Function to install necessary tools if not present
install_if_not_present() {
  local package=$1
  if ! command -v "$package" &>/dev/null; then
    echo "Installing $package..."
    if ! pacman -Sy --noconfirm "$package"; then
      echo "Failed to install $package. Exiting."
      exit 1
    fi
  fi
}

# Install parted if not installed
install_if_not_present parted

# Identify the disk
echo "Listing all disks and partitions:"
lsblk

read -p "Enter the disk name (e.g., /dev/nvme0n1 or /dev/sda): " disk
read -p "Enter the EFI partition (e.g., ${disk}p1): " efi_partition
read -p "Enter the Btrfs partition (e.g., ${disk}p2): " btrfs_partition

# Check if the provided disk and partitions exist
if [[ ! -b "$disk" ]] || [[ ! -b "$efi_partition" ]] || [[ ! -b "$btrfs_partition" ]]; then
  echo "Invalid disk or partition. Exiting."
  exit 1
fi

# Prepare mount points
echo "Preparing mount points..."
sudo mkdir -p /mnt
sudo mkdir -p /mnt/boot/efi

# Mount the Btrfs root subvolume
echo "Mounting Btrfs root subvolume..."
sudo mount -o subvol=@,compress=zstd,noatime "$btrfs_partition" /mnt

# Mount the EFI partition
echo "Mounting EFI partition..."
sudo mount "$efi_partition" /mnt/boot/efi

# Bind mount necessary directories
echo "Bind mounting necessary directories..."
for dir in /dev /dev/pts /proc /sys /run; do
  sudo mount --bind $dir /mnt$dir
done

# Chroot into the system
echo "Chrooting into the system..."
sudo arch-chroot /mnt

# Instructions for the user
echo "You are now in the chroot environment."
echo "Please reinstall GRUB and generate its configuration:"
echo "  grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB"
echo "  grub-mkconfig -o /boot/grub/grub.cfg"
echo "Afterwards, type 'exit' to leave chroot and then reboot your system."
echo "-----------------------------------------------------"
echo "Script executed on $(date)"
echo "Thank you for using this script!"
